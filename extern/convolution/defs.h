#ifndef DEFS_H
#define DEFS_H

#include <fftw3.h>

typedef double real;
typedef fftw_complex cmplx;

#define IDX(i,j,ld) (((i)*(ld))+(j))

typedef struct {
  int m;  //rows
  int n;  //cols
} size;

typedef struct {
  real *mat;
  size size;
} matrix;

typedef struct {
  cmplx *mat;
  size size;
} cmatrix;

typedef struct {
  char *id;
  char *cnv_type;
  size offset;
  size sx;
  size sy;
  size sf;
  size sz;
  size sfft;
  size sp;
  cmatrix *fft;
  fftw_plan plan_fwd;
  fftw_plan plan_bwd;
} cnvmat;

typedef void (*function)(double *x, double * f, double * gradient, void * data);
typedef matrix * (*forward_function)(void * model, matrix * input);
typedef matrix * (*transpose_function)(void * model, matrix * input);

typedef struct {
  matrix * data;
  void * model;
  forward_function forward;
  transpose_function transpose;
} generative_model;

#endif

