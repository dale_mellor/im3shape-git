ABOUT IM3SHAPE
--------------

Im3shape measures the shapes of galaxies in astronomical survey images, taking
into account that they have been distorted by a point-spread function.

If you have any difficulty getting, installing, or using im3shape then please
get in touch: joseph.zuntz@manchester.ac.uk


USING IM3SHAPE
--------------

1. First, install im3shape according to the instructions in the INSTALLATION
file.   This will build the program bin/im3shape

2. The syntax for im3shape is:
./bin/im3shape param_file image_file cat_file psf_file output_file 
               [first_image] [last_image] [options] 

These arguments are:
 - param_file :  An ini file containing the main options for im3shape.
 - image_file :  The FITS file containing the galaxies to process
 - cat_file   :  Text file catalog of galaxy locations ID x y
 - psf_file   :  Text file catalog of PSFs at galaxy locations  beta fwhm e1 e2
                 Depending on ini file options this can also be an image file.
 - output_file:  File where output catalog is written

 Optional:
  - first_image:
  - last_image : only process this range of galaxies from the catalog
  - options    : Any ini file options can also be set on the command line in
                 the format name=value (no spaces).

 3. Try the example in the example directory. Run:
cd example
python ../bin/im3shape.py params.ini  image.fits  catalog.txt psf.txt  out.txt 0 9

This will analyze the first 10 galaxies in the file image.fits at positions
from catalog.txt assuming a PSF for each image specified in psf.txt with the
settings from params.ini.  The results will be saved to out.txt.


4. Your own parameter file.

There are a number of parameters in im3shape, but all have a default value which
is (usually) sensible.  The file ini/i3_options.ini describes what they all mean.
(This file is generated during the build process so will not appear until then).

SPECIAL NOTE ON RUNNING ON CLUSTERS/SUPERCOMPUTERS
--------------------------------------------------

Make sure the file specified by sersic_lookup_file in the ini file is accessible 
by the running program if you are using the sersic model.  If this is not possible
then you can set use_hankel_transform_trick=F to disable.


GENERATING IMAGES WITH IM3SHAPE
-------------------------------

As well as the standard mode in which it measures the shapes of galaxies, 
im3shape can generate galaxy shape images so you can see what kind of model is
being fitted to the data.

Try this from the "example" directory:
../bin/im3shape --generate params.ini stamp.fits

This will generate a file stamp.fits containing a default image, with the
resolution, size, and model chosen in params.ini.  The specific model
parameters, like radius and ellipticity, are also set from the "start"
parameters in that file.  The default PSF is a small, circular Moffat profile.

You can alter the parameters of the generated model with further command line
options, of the form "param=value" (no spaces are allowed).

The parameters that are accepted are:
 - any that can be set in the ini file 
     - see ini/i3_options.ini

 - model parameters, set directly.  For the default sersics model:
     - e1, e2, x0, y0, radius, bulge_A, disc_A
     - radius_ratio, bulge_index, disc_index, delta_theta_bulge, delta_e_bulge
     
- the Moffat PSF parameters:
     - psf_fwhm, psf_beta, psf_e1, psf_e2   



