#include "i3_image_fits.h"
#include "i3_xray.h"
#include "i3_math.h"
#include "i3_model_tools.h"
#include "i3_xray_definition.h"
#include "i3_options.h"

i3_flt i3_xray_likelihood(i3_image * model_image, i3_xray_parameter_set * p, i3_data_set * dataset)
{

	i3_xray_parameter_set * p_beermat = malloc(sizeof(i3_xray_parameter_set));
	*p_beermat = *p;
	i3_e12_to_e12beermat(p->e1, p->e2, 0.95, &(p_beermat->e1), &(p_beermat->e2));

	//Generate the model image
	i3_xray_model_image(p, dataset, model_image);
	i3_flt like;

	if (dataset->options->poisson_errors){
		// like = i3_poisson_like_exposure(model_image, dataset->image, dataset->weight);
		like = i3_poisson_like_exposure_background(model_image, dataset->image, dataset->weight, 0.0);
	}
	else{
		i3_flt chi2 = i3_chi2_weight_map(model_image, dataset->image, dataset->weight );
		like = -chi2/2.0;
	}
	//Use the weight map to get chi^2
	// Return log-like
	if (isnan(like)) like = -1e10;
	// printf("like = %le\n", like);
	return like;

}

void i3_xray_model_image(i3_xray_parameter_set * p, i3_data_set * dataset, i3_image * model_image)
{
	// Get the image size parameters
        
    int n_sub = dataset->upsampling;
    int n_pix = dataset->stamp_size;
    int n_pad = dataset->padding;
    int n_all = n_pix+n_pad;

    // Make an empty high-res image
    i3_image * image_hires = i3_image_create(n_all*n_sub,n_all*n_sub);
    i3_image_zero(image_hires);


	// Add a King profile with params upsampled
    i3_flt rc = p->rc * n_sub;
    i3_flt x0 = p->x0 * n_sub;
    i3_flt y0 = p->y0 * n_sub;
	i3_add_real_space_beta_profile(rc, p->e1, p->e2, p->A, p->beta, x0, y0, image_hires);


	// Convolve with the PSF and down-sample into the model image
	i3_image_convolve_fourier( image_hires, dataset->psf_downsampler_kernel );
	int cut_start = (n_pad/2)*n_sub + n_sub/2;
	int cut_step = n_sub;
	i3_image_dsample_cut_into( image_hires, model_image, cut_start, cut_start, cut_step);

	//Free hires image
	i3_image_destroy(image_hires);

}



