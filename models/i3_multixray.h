#ifndef _H_I3_MULTIXRAY
#define _H_I3_MULTIXRAY
#include "i3_multixray_definition.h"
#include "i3_image.h"
#include "i3_data_set.h"
#include "i3_options.h"

#define ARCSEC2DEG 0.000277777777777778

i3_flt i3_multixray_likelihood(i3_image * model_image, i3_multixray_parameter_set * parameters, i3_data_set * dataset);
void i3_multixray_model_image(i3_multixray_parameter_set * p, i3_data_set * dataset, i3_image * model_image, int exposure_index);

void i3_multixray_map(i3_multixray_parameter_set * input, i3_multixray_parameter_set * output, i3_options * options);

#endif
