name="xray"

parameters = ['x0','y0','e1','e2','rc', 'A', 'beta']
fixed_by_default = []

pi=3.141593

types = {
	'x0':float,
	'y0':float,
	'e1':float,
	'e2':float,
	'rt':float,
	'rc':float,
	'A':float,
	'beta':float,
}

widths = {
	'x0':0.1,
	'y0':0.1,
	'e1': 0.1, 
	'e2': 0.2,
	'rt':0.1,
	'rc':0.1,
	'A': 0.3,
	'beta': 0.3,
}

starts = {
	'x0':20. ,
	'y0':20. ,
	'e1' : 0.0, 
	'e2': 0.0,
	'rt': 20.0,
	'rc': 2.0,
	'A': 1.0,
	'beta': 2.0/3.,
	
}

# The ones below this point are optional, but encouraged

min = {
	'x0': -20.0,
	'y0': -20.0,
	'e1': -0.95,
	'e2': -0.95,
	'rt': 0.2,
	'rc': 0.1,
	'A': 0.,
	'beta': 1.0/6.0,
	
}

max = {
	'x0': 60.0,
	'y0': 60.0,
	'e1': 0.95, 
	'e2': 0.95,
	'rt': 100.,
	'rc': 50,
	'A': 10.,
	'beta': 3.0,
}


help = {
	'x0': 'The x-coordinate of the center of the de Vaucouleurs bulge profile',
	'y0': 'The y-coordinate of the center of the de Vaucouleurs bulge profile',
	'e1': 'The ellipticity along x axs', 
	'e2': 'The ellipticity along x=y axs',
	'rt': 'The (transitition?) radius r_t',
	'rc':'The outer radius r_c',
	'A': 'Amplitude of the profile',	
}

proposal = "i3_covariance_matrix_proposal"
# start_function = "i3_xray_start"
# get_model_image_function = "i3_xray_model_image"


