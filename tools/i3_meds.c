#include "i3_meds.h"
#include "levmar/levmar.h"

i3_data_set * i3_build_multiple_exposure_dataset(i3_options * options, 
        gal_id identifier, int n_exposure, i3_image ** images, i3_image ** weights,
        i3_image ** psf_images, i3_transform * transforms, char * bands){
    
    if (n_exposure>MAX_N_EXPOSURE){
        I3_FATAL("Sorry - you have more exposures than I can cope with.  Recompile changing MAX_N_EXPOSURE", 666);
    }


    i3_data_set * data_set = (i3_data_set*)malloc(sizeof(i3_data_set));
    data_set->identifier=identifier;
    data_set->psf_form = NULL;
    data_set->psf_fwhm = 0.0;
    data_set->noise=options->noise_sigma;
    data_set->central_pixel_upsampling=options->central_pixel_upsampling;
    data_set->n_central_pixel_upsampling=options->n_central_pixel_upsampling;
    data_set->n_central_pixels_to_upsample=options->n_central_pixels_to_upsample;
    data_set->stamp_size=options->stamp_size;
    data_set->upsampling=options->upsampling;
    data_set->padding=options->padding;
    data_set->start_params = NULL;
	data_set->options=options;
    i3_dataset_reset_counters(data_set);
	data_set->levmar_info = malloc(sizeof(i3_flt)*LM_INFO_SZ);

    // Create exposure
    data_set->n_exposure = n_exposure;
    for (int i=0; i<n_exposure; i++){
        i3_exposure * exposure = &(data_set->exposures[i]);
        exposure->image = images[i];
        exposure->weights = weights[i];
        exposure->kernel = malloc(n_exposure * sizeof(i3_fourier*));
        exposure->transform= transforms[i];
        if (bands){
            exposure->band = bands[i];
        }
        else{
            exposure->band = '?';
        }

    }
    int i;
    // Determine the size of the full tiled image, which is going into data_set->image
    // for compatibility with single exposure mode.
    int total_width = 0;
    int max_height = 0;
    for (i=0; i<n_exposure; i++){
        total_width += images[i]->nx;
        max_height = max_height > images[i]->ny ? max_height : images[i]->ny; 
    }

    data_set->image = i3_image_create(total_width, max_height);
    data_set->weight = i3_image_create(total_width, max_height);
    i3_tile_images(data_set->image, images, n_exposure);
    i3_tile_images(data_set->weight, weights, n_exposure);
    for (i=0; i<data_set->n_exposure; i++){
        data_set->exposures[i].kernel = i3_fourier_conv_kernel(images[i]->nx,options->upsampling, options->padding, psf_images[i], options->perform_pixel_integration);
    }
    for (i=0; i<MAX_N_EXPOSURE; i++){
        i3_exposure * exposure = &(data_set->exposures[i]);
        exposure->x = 0.0;
        exposure->y = 0.0;
        exposure->e1 = 0.0;
        exposure->e2 = 0.0;
        exposure->chi2 = 0.0;
        exposure->residual_stdev = 0.0;
    }

    data_set->covariance_estimate = NULL;


    return data_set;
}



void i3_multiple_exposure_dataset_destroy(i3_data_set * dataset)
{
    //Just free the psf kernel - all else is owned by someone else
    for (int i=0; i<dataset->n_exposure; i++){
        i3_fourier_destroy(dataset->exposures[i].kernel);
    }
    i3_image_destroy(dataset->image);
    i3_image_destroy(dataset->weight);
    free(dataset->levmar_info);
    dataset->image=NULL;
    dataset->PSF=NULL;
    dataset->weight=NULL;
    dataset->psf_downsampler_kernel=NULL;
    dataset->psf_form=NULL;
    dataset->start_params=NULL;
    dataset->options=NULL;
    free(dataset);
}
