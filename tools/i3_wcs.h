#ifndef _H_I3_WCS
#define _H_I3_WCS
#include "i3_global.h"
#include "i3_math.h"


/*
 * Here we define the stepsize (in the DES WCS standard units of degrees) to use when estimating the
 * locally linear transformation matrix at the position of our postage stamp.  This is currently
 * 5 DECAM pixels = 1.35 arcsec
 */
#define I3_WCS_STEP 0.000375
#define I3_IMAGE_STEP 5.


typedef struct i3_transform{

  i3_flt ra0;
  i3_flt dec0;
  i3_flt cosdec0;
  i3_flt x0;
  i3_flt y0;
  i3_flt A[2][2];

} i3_transform; // this stores a 2D transformation matrix (x0,y0,A11,A12,A21,A22)

i3_transform get_null_transform();


void i3_wcs_to_image(i3_flt ra, i3_flt dec, i3_flt e1_w, i3_flt e2_w, i3_flt ab_w, i3_transform T, 
                     i3_flt *x, i3_flt *y, i3_flt *e1, i3_flt *e2, i3_flt *ab);

void i3_image_to_wcs(i3_flt x, i3_flt y, i3_flt e1_p, i3_flt e2_p, i3_flt ab_p, i3_transform T, 
                     i3_flt *ra, i3_flt *dec, i3_flt *e1, i3_flt *e2, i3_flt *ab);

// struct wcsprm * i3_get_wcsprm_from_file(char * filename, int hdu);

#endif
