#include "i3_image.h"
#include "i3_data_set.h"

int i3_max_number_exposures = MAX_N_EXPOSURE;

void i3_dataset_reset_counters(i3_data_set * dataset){
	
	dataset->n_logL_evals=0;
	dataset->n_logL_excluded=0;
	dataset->n_logL_warning=0;
}

void i3_dataset_destroy(i3_data_set * dataset)
{
	//Just free the psf kernel - all else is owned by someone else
	i3_fourier_destroy(dataset->psf_downsampler_kernel);
	free(dataset->levmar_info);
	dataset->image=NULL;
	dataset->PSF=NULL;
	dataset->weight=NULL;
	dataset->psf_downsampler_kernel=NULL;
	dataset->psf_form=NULL;
	dataset->start_params=NULL;
	dataset->options=NULL;
	if (dataset->covariance_estimate) free(dataset->covariance_estimate);
	free(dataset);
}

int i3_dataset_count_distinct_bands(i3_data_set * dataset){
	int n = 0;
	int counts[256] = {0};  // zeroed array of length up to the max of char
	for (int i=0; i<dataset->n_exposure; i++){
		unsigned char b = (unsigned char)dataset->exposures[i].band;
		if (counts[b]==0){
			counts[b]++;
			n++;
		}
	}
	return n;
}


void i3_dataset_get_distinct_bands(i3_data_set * dataset, int nband, char * bands){
	int n = dataset->n_exposure;
	if (n==0) return;
	int B[n];
	for (int i=0; i<n; i++) B[i] = dataset->exposures[i].band;
	i3_array_int_sort(B, n);


    int j=1;
    bands[0] = B[0];
    for (int i=1; i<n; i++){
    	int b = B[i];
        if(b!=bands[j-1]){
            bands[j]=b;
        	j++;
        }    	
    }

}

