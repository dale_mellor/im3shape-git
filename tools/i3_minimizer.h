#ifndef _H_I3_MINIMIZER
#define _H_I3_MINIMIZER
#include "i3_model.h"
#include "i3_data_set.h"
#include "gsl/gsl_multimin.h"
#include "i3_options.h"

typedef struct i3_minimizer{
	i3_model * model;
	i3_image * model_image;
	i3_data_set * data_set;
	// gsl_multimin_fminimizer * minimizer;
	// gsl_multimin_function * function_wrapper;
	i3_flt best_like;
	int status;
	i3_parameter_set * step_size;
	int iterations;
	int nparam;
	int max_iterations;
	i3_flt tolerance;
	i3_flt gradient_like;
	int verbosity;
	i3_parameter_set * start; /* We want to include this so that any parameters not set use their default value.*/
	i3_flt *lower_flt, *upper_flt, *scale_factor;
	i3_options * options;
	i3_flt * covariance_estimate;
	bool has_covariance_estimate;
} i3_minimizer;

typedef enum {i3_minimizer_success, i3_minimizer_error, i3_minimizer_no_converge} i3_minimizer_result;
typedef enum {
	i3_minimizer_method_levmar=0,
	i3_minimizer_method_simplex=1, 
	i3_minimizer_method_powell=2, 
	i3_minimizer_method_praxis=3, 
	i3_minimizer_method_golden=4,
} i3_minimizer_method;

i3_minimizer * i3_minimizer_create(i3_model * model, i3_options * options);
i3_parameter_set * i3_minimizer_run(i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set,i3_data_set * data_set, i3_minimizer_method method);
void i3_minimizer_destroy(i3_minimizer * minimizer);


void i3_minimizer_vector_to_pointer(i3_minimizer * minimizer, const gsl_vector * v, i3_parameter_set * x);
gsl_vector * i3_minimizer_pointer_to_vector(i3_minimizer * minimizer, i3_parameter_set * x);

//Regular Methods
i3_parameter_set * i3_minimizer_run_simplex(i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);
i3_parameter_set * i3_minimizer_run_golden (i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);
i3_parameter_set * i3_minimizer_run_praxis (i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);
i3_parameter_set * i3_minimizer_run_powell (i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);
i3_parameter_set * i3_minimizer_run_levmar (i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);

//Derivative methods
i3_parameter_set * i3_minimizer_with_derivatives(i3_minimizer * minimizer, i3_parameter_set * initial, i3_data_set * data_set);
i3_parameter_set * i3_minimizer_run_lbf(i3_minimizer * minimizer, i3_parameter_set * initial_parameter_set, i3_data_set * data_set);
void i3_minimizer_setup_model_image(i3_minimizer * minimizer, i3_image * data_image);


#endif
