#ifndef _H_I3_MCMC
#define _H_I3_MCMC
#include "i3_global.h"
#include "i3_options.h"
#include "i3_image.h"
#include "i3_model.h"
#include "i3_data_set.h"

/**
 * An object corresponding to a single run of an MCMC chain.
 * The general pattern is to create the i3_mcmc
 * Possibly tweak its parameters from the defaults taken from the options
 * Then run it from a specified start position
 * Then extract the results as required.
**/
typedef struct i3_mcmc {
	i3_model * model; /**< The model object being fit*/
	int nParam; /**< The number of varied parameters (fixed parameters are not counted)*/
	i3_flt * cov; /**< The proposal covariance for the fit parameters - not currently used*/
	i3_flt * cov_sqrt;
	i3_flt * random_rotation; /**< A random rotation used to adjust the proposal a la CosmoMC*/
	i3_parameter_set * samples; /**< An array of the generated samples*/
	i3_parameter_set * width; /**< The proposal widths */
	i3_flt * likelihoods; /**< An array of the log-likelihoods of the generated samples */
	i3_flt scaling_parameter; /**< The parameter that scales the proposal from the estimated covariance (standard value 2.4)*/
	i3_image * model_image; /**< A model image of the last model generated (if created by the likelihood function)*/
	int nSamples; /**< The number of samples generated */
	int nAccept; /**< The number of accepted proposals*/
	char * acceptances;
	i3_flt current_likelihood; /**< The likelihood of the current (or most recent) point*/
	i3_flt proposed_likelihood; /**< The likelihood of the proposed point */
	bool propose_rotation; /**< Whether or not to use a proposal rotation scheme as in CosmoMC*/
	i3_flt temperature; /**< The temperature at which to MCMC */
	char * filename_base;
} i3_mcmc;

typedef void (*i3_mcmc_callback_function)(i3_mcmc*,i3_data_set*,i3_options*);

/**
 * Create an MCMC object.
 * \param model The likelihood model used for the object
 * \param options The option set used to initialize the MCMC.  The number of samples, stamp size, proposal scaling factor, and rotation option are taken from it at the moment.  The widths are currently not set - you need to set the manually.
**/
i3_mcmc * i3_mcmc_create(i3_model * model, i3_options * options);

/**
 * Free the memory associated with the MCMC
 * \param mcmc The MCMC to destroy.
**/
void i3_mcmc_destroy(i3_mcmc * mcmc);

/**
 * INTERNAL After a proposal and evaluation of a chain step, records the new current point.
 * \param mcmc The MCMC being used.
 * \param current_parameters The newly accepted chain step.  Might be the same as the previous one, or the newly accepted point.
 * \param current_likelihood The corresponding likelihood of the parameter set.
**/
void i3_record_parameters(i3_mcmc * mcmc,i3_parameter_set * current_parameters,i3_flt current_likelihood);

/**
 * Run the MCMC and generate the chain.  Note that you can also run with a function run after every sample using i3_mcmc_run_callback
 * \param mcmc The MCMC to run - should already be initialized
 * \param initial_parameters The starting point for the chain
 * \param data The data set to analyze (passed to the likelihood function)
 * \param options The option set describing the run.  The number of samples and burn-in period are taken from this.
**/
int i3_mcmc_run(i3_mcmc * mcmc, i3_parameter_set * initial_parameters, i3_data_set * data, i3_options * options);

/**
 * Save the MCMC results to a text file.
 * \param mcmc The chain.
 * \param filename The name of the file to which to save.
**/
void i3_mcmc_save_results(i3_mcmc * mcmc, char * filename);

void i3_mcmc_copy_results(i3_mcmc * mcmc, i3_parameter_set * buffer, int nsample_max, int * nsample);
i3_parameter_set * i3_mcmc_last_sample(i3_mcmc * mcmc);
void i3_mcmc_compute_statistics(i3_mcmc * mcmc, i3_parameter_set * means, i3_parameter_set * stdevs);
i3_flt i3_mcmc_acceptance_rate(i3_mcmc * mcmc);
i3_parameter_set * i3_mcmc_best_sample(i3_mcmc * mcmc);
int i3_mcmc_best_index(i3_mcmc * mcmc);
i3_parameter_set * i3_mcmc_sample_number(i3_mcmc * mcmc, int n);
void i3_mcmc_anneal_callback(i3_mcmc * mcmc, i3_data_set * data_set, i3_options * options);
void i3_mcmc_anneal(i3_mcmc * mcmc, i3_parameter_set * initial_parameters, i3_data_set * data, i3_options * options);
int i3_mcmc_run_callback(i3_mcmc * mcmc, i3_parameter_set * initial_parameters, i3_data_set * data, i3_options * options, i3_mcmc_callback_function callback);
i3_flt i3_mcmc_last_like(i3_mcmc *mcmc);
void i3_mcmc_adaptive_callback(i3_mcmc * mcmc, i3_data_set * data_set, i3_options * options);

i3_flt i3_mcmc_like_number(i3_mcmc * mcmc, int n);
void i3_mcmc_setup_image(i3_mcmc * mcmc, i3_image * data_image);

#endif



