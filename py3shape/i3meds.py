
#i3meds.py
import meds
import os
import numpy as np
from . import utils
import collections
import warnings
from . import des_utils
import yaml
from .errors import *


DEFAULT_OPTIONS = dict(
starting_params_only=False,
rescale_weight=False,
first_exposure=1,
max_nexposures=None, 
use_nearest_pix_mask=False,
reject_outliers=True,
replace_psf_path=None,
psfex_rerun_version=None,
psf_input_method='psfex',
)

DES_ROWMAX = 4096
DES_COLMAX = 2048

class I3MultiExposureInputs(object):
    def __init__(self):
        self.exposures = []
        self.meta = {}

    def __getitem__(self, n):
        return self.exposures[n]

    def __len__(self):
        return len(self.exposures)

    def __iter__(self):
        return iter(self.exposures)

    def all(self, name):
        return [info[name] for info in self.exposures]

    def append(self, exposure):
        self.exposures.append(exposure)

    @classmethod
    def combine(cls, inputs):
        out = cls()
        out.exposures = sum([x.exposures for x in inputs], [])
        out.meta = inputs[0].meta.copy()
        out.meta['n_band'] = len(inputs)
        out.meta['bands'] = ''.join(x.meta['bands'] for x in inputs)
        out.meta['nreject'] = sum(x.meta['nreject'] for x in inputs)

        #no longer a useful concept
        if "iobj" in out.meta:
            del out.meta['iobj']

        return out



    def __add__(self, other):
        "Combine with another set of inputs"
        out = I3MultiExposureInputs()
        n_self = len(self)
        n_other = len(other)

        out.exposures = self.exposures + other.exposures
        out.meta = self.meta.copy()
        out.meta.update(other.meta)

        # Special pieces of metadata that
        if 'nband' in out.meta:
            out.meta['nband'] = self.meta['nband'] + out.meta['nband']
        self.assert_same(out, 'ID')
        self.assert_same(out, 'tilename')

        #No longer a useful concept
        out.meta['iobj'] = -1

        #Get the bands of all the exposures.
        #This should just build up the list
        out.meta['band'] += self.meta['band'] + other.meta['band']

class TilingSplit(object):
    def __init__(self,filename):
        import fitsio
        f = fitsio.FITS(filename)
        tiling=f[1].read_column('tiling')
        exposure=f[1].read_column('expnum')
        order = exposure.argsort()
        self.tiling = tiling[order]
        self.exposure=exposure[order]
        f.close()

    def get_tiling(self, exposure):
        index = self.exposure.searchsorted(exposure)
        if index>=len(self.exposure) or self.exposure[index]!=exposure:
            raise I3MissingTiling("Unknown exposure {} in tiling file".format(exposure))
        return self.tiling[index]

    def tiling_is_even(self, exposure):
        n = self.get_tiling(exposure)
        return (n%2)==0

#Global as this will be slow to load in.
TILING_SPLIT = None


BLACKLIST_CACHE = {}
PSFEX_CACHE = {}
PSFEX_CACHE_FILENAME = ""
PSF_DISTORTION_SPLINE_CACHE = {}

class BlacklistedPSFClass(object):
    pass

BlacklistedPSF = BlacklistedPSFClass()


class I3MEDS(meds.MEDS):
    def __init__(self, filename, options):

        self.options=options
        blacklist_file = self.options['blacklist']
        psf_spline_file = self.options['psf_spline_file']

        super(I3MEDS, self).__init__(filename)
        self.saved_filename = filename

        #Clear the cache if we have moved onto the next MEDS file
        #I found that loading the PSFs from file
        #is taking about 15-20% of the run time on 
        #edison.  So I'm caching them
        global PSFEX_CACHE
        global PSFEX_CACHE_FILENAME
        global TILING_SPLIT
        #Clear the cache if we've finished one meds file,
        #just so it doesn't grow indefinitely
        if self.saved_filename != PSFEX_CACHE_FILENAME:
            PSFEX_CACHE = {}
            PSFEX_CACHE_FILENAME = self.saved_filename

        if (TILING_SPLIT is None) and self.options.tiling_split_file:
            TILING_SPLIT = TilingSplit(self.options.tiling_split_file)

        #This is only relevant for GREAT-DES
        #In the Great-DES scheme we just look up the index
        #of the psf to use in the truth table
        self.truth_filename = self.saved_filename.replace(".meds.", ".truth.").replace(".noisefree","")          
        self.psf_id_col = None
        self.tilename = utils.find_tilename(self.saved_filename)
        self.blacklist = self.read_blacklist(blacklist_file)
        self.psf_distortion = self.read_psf_distortion_splines(psf_spline_file)
        if "config" in self.get_meta().dtype.names:
            self.config_info = yaml.load(self.get_meta()['config'][0])
        self.band = self.get_band()
            
    def get_band(self):
        "Determine the band this file comes from"
        if hasattr(self, "config_info"):
            return self.config_info['band']
        try:
#            band = self.get_meta()['config'][0]
            band = self.get_meta()['coadd_file'][0].split('.')[0].split('_')[-1]
            assert (band and (band in 'grizy') and (len(band)==1))
        except (IndexError, AssertionError, ValueError):
            warnings.warn("Warning - using fake band r")
            band = '?'
        return band

    def read_psf_distortion_splines(self, psf_spline_file):
        if not psf_spline_file:
            return None
        if psf_spline_file not in PSF_DISTORTION_SPLINE_CACHE:
            import cPickle
            print "Loading delta e psf spline file ", psf_spline_file
            PSF_DISTORTION_SPLINE_CACHE[psf_spline_file] = cPickle.load(open(psf_spline_file))
        return PSF_DISTORTION_SPLINE_CACHE[psf_spline_file]


    def read_blacklist(self, blacklist_file):
        if blacklist_file in BLACKLIST_CACHE:
            return BLACKLIST_CACHE[blacklist_file]

        blacklist = set()

        if not blacklist_file:
#            print "No blacklist"
            return blacklist

        for line in open(blacklist_file):
            line=line.strip()
            if not line: continue
            words = line.split()
            exposure = words[1]
            ccd = words[2]
            exposure_ccd = exposure+"_"+ccd
            blacklist.add(exposure_ccd)

        BLACKLIST_CACHE[blacklist_file] = blacklist

        print "Found %d objects in blacklist file %s" % (len(blacklist), blacklist_file)

        return blacklist

    def get_psfex_psf(self, iobj, iexp):
        desdata_old=self.get_meta()['DESDATA'][0]
        desdata_new=os.environ['DESDATA']

        source_path = self.get_source_path(iobj, iexp)
        source = os.path.split(source_path)[1].split('.')[0]

        if source in self.blacklist:
            return BlacklistedPSF

        psf_path = source_path.replace(
            '.fits.fz','_psfcat.psf').replace(
            desdata_old,desdata_new)

        # fiddle with the PSF path, in one of two ways,
        # a replacement or a specific new dir
        if self.options.replace_psf_path:
            psf_path = psf_path.replace(self.options.replace_psf_path.split(',', maxsplit=1))

        #specific new dir for re-run psfs
        if self.options.psfex_rerun_version:
            psf_path = psf_path.replace("OPS", "EXTRA")
            psf_path = psf_path.replace("red/DECam", "psfex-rerun/{}/DECam".format(
                self.options.psfex_rerun_version))


        #Load the PSFEx data
        import galsim.des
        try:
            psfex_i = galsim.des.DES_PSFEx(psf_path)
        except:
            print "PSF path missing", psf_path
            return None

        #Get the image array
        return self.extract_psfex_psf(psfex_i, iobj, iexp)

    def get_bundled_psfex_name(self, iobj, iexp):
        source_path = self.get_source_path(iobj, iexp)
        source = os.path.split(source_path)[1].split('.')[0]

        #PSF blacklist
        if source in self.blacklist:
            if self.options.verbosity>2:
                print "%s blacklisted" % source
            return BlacklistedPSF
        hdu_name = "psf_"+self.options.psfex_rerun_version+"_"+source+"_psfcat"
        return hdu_name        

    def get_collected_psfex_psf(self, iobj, iexp, return_profile=False):
        filename = self.options.psf_bundle_path.format(tilename=self.tilename, band=self.band)
        return self.get_fits_file_psfex_psf(filename, iobj, iexp, return_profile)

    def get_bundled_psfex_psf(self, iobj, iexp, return_profile=False):
        return self.get_fits_file_psfex_psf(self._filename, iobj, iexp, return_profile)


    def get_fits_file_psfex_psf(self, filename, iobj, iexp, return_profile=False):
        hdu_name = self.get_bundled_psfex_name(iobj, iexp)
        if hdu_name is BlacklistedPSF:
            return BlacklistedPSF
        psfex_i = self.get_psfex_psf_from_file(filename, iobj, iexp, hdu_name)
        return self.extract_psfex_psf(psfex_i, iobj, iexp, return_profile=return_profile)



    def get_psfex_psf_from_file(self, filename, iobj, iexp, hdu_name):
        if hdu_name is BlacklistedPSF:
            return BlacklistedPSF

        #Maybe we have it in the cache already
        if hdu_name in PSFEX_CACHE:
            psfex_i = PSFEX_CACHE[hdu_name]
            if psfex_i is None:
                return None
        else:
            #Turn the HDU into a PSFEx object
            #PSFEx expects a pyfits HDU not fitsio.
            #This is insane.  I know this.
            import galsim.des
            from astropy.io import fits as pyfits
        
            try:
                hdu = pyfits.open(filename)[hdu_name]
            except KeyError:
                PSFEX_CACHE[hdu_name] = None
                if self.options.verbosity>3:
                    print "Could not find HDU %s in %s" % (hdu_name, self._filename)
                return None

            try:
                psfex_i = galsim.des.DES_PSFEx(hdu)
            except IOError:
                print "PSF bad but not blacklisted: %s in %s"%(hdu_name, self._filename)
                psfex_i = None
                
            PSFEX_CACHE[hdu_name] = psfex_i
        
        if psfex_i is None:
            return None

        return psfex_i



    def extract_psfex_psf(self, psfex_i, iobj, iexp, return_profile=False):
        if psfex_i is None:
            return None

        psf_size=(self.options.stamp_size+self.options.padding)*self.options.upsampling
        # locations in PSF data at which to interpolate
        orig_col = self['orig_col'][iobj][iexp]
        orig_row = self['orig_row'][iobj][iexp]

        if self.psf_distortion is None:
            distort_function = None
        else:
            ccd = self.get_ccd_info(iobj, iexp)[0]
            #CHECK THE ORDERING OF THESE!
            x, y = des_utils.ccd_pixel_to_field_arcsec(ccd, orig_row, orig_col)
            #And also the sign of this!
            delta_e1 = -self.psf_distortion[0](x,y)[0,0]
            delta_e2 = -self.psf_distortion[1](x,y)[0,0]

            #convert delta_e1 and e2 to pixel coordinates
            #but they are already in pixel coordinates.
            #This is the lazy of doing this - we will just flip e2
            #approximating the WCS as a simple flip. This is very wrong
            #in general but for a delta might be just about okay
            delta_e2 *= -1

            def distort_function(psf):
                #need to convert the orig_row and orig_col into FOV coordinates
                #and then 
                psf = psf.shear(g1=delta_e1, g2=delta_e2)
                return psf

        psf = utils.getPSFExarray(psfex_i, orig_col, orig_row,
                        psf_size, psf_size, self.options.upsampling, 
                        return_profile=return_profile,
                        distort_function=distort_function)

        return psf

    def get_fixed_gaussian_psf(self, iobj, iexp):
        "This is slow - for test runs only."
        import galsim
        psf_size=(self.options.stamp_size+self.options.padding)*self.options.upsampling        

        #Get the localized WCS information
        wcs_path = self.get_source_info(iobj,iexp)[3].strip()
        orig_col = self['orig_col'][iobj][iexp]
        orig_row = self['orig_row'][iobj][iexp]        
        image_pos = galsim.PositionD(orig_col,orig_row)
        wcs = galsim.FitsWCS(wcs_path)
        local_wcs = wcs.local(image_pos)

        local_wcs._dudx /= self.options.upsampling
        local_wcs._dudy /= self.options.upsampling
        local_wcs._dvdx /= self.options.upsampling
        local_wcs._dvdy /= self.options.upsampling
        local_wcs._det /= self.options.upsampling**2

        #Generate the PSF in sky coordinates
        pix = wcs.toWorld(galsim.Pixel(0.27), image_pos=image_pos)
        psf_sky = galsim.Convolve([galsim.Gaussian(fwhm=self.options.fixed_gaussian_fwhm), pix])
        psf_sky = galsim.Gaussian(fwhm=self.options.fixed_gaussian_fwhm)
        psf_stamp = psf_sky.drawImage(wcs=local_wcs, nx=psf_size, ny=psf_size, method='no_pixel')
        psf_stamp_array = psf_stamp.array.copy()
        assert psf_stamp_array.shape == (psf_size, psf_size)
        return psf_stamp.array.copy()


    def get_fake_psf(self, iobj, iexp):
        psf_size=(self.options.stamp_size+self.options.padding)*self.options.upsampling
        logger.warning("Warning: Using false PSF")
        psf = utils.small_round_psf_image(psf_size) 
        return psf

    def get_great_des_psf(self, iobj, iexp):
        import pyfits
        #only load the table once per process
        if self.psf_id_col is None:
            self.psf_id_col = pyfits.getdata(self.truth_filename)['id_psf']
        #find and load the corresponding PSF image,
        #again from a fixed filename
        psf_id = self.psf_id_col[iobj]
        psf_filename = os.path.split(self.truth_filename)[0]+ "/nbc.psf.hires.nsub%d.fits"%self.options.upsampling
        if not os.path.exists(psf_filename):
            raise I3MissingPSFFile("Could not find PSF file for specified upsampling: %s"%psf_filename)
        #load psf, just once since single exposure
        psf  = pyfits.getdata(psf_filename, psf_id)
        return psf

    def get_psf(self, iobj, iexp):
        """
        @brief get a list of numpy arrays of PSF images for each exposure
        @use_psfex if yes, use the PSFEx files, otherwise use dummy PSF
        @iobj object id in the meds file
        @selection list of indices of cutouts to use
        @return list of numpy arrays with images of PSFs
        """
        psf_input_method = self.options.psf_input
        # get the exposure images list, if there is a coadd (n_exposures>1) then skip it
        # n_cutout - number of available cutouts
        # n_image - number of single exposure images, or 1 if n_cutout==1
        # index_first_image - 0 if n_cutout==1 and 1 if n_cutout!=1
        psf_size=(self.options.stamp_size+self.options.padding)*self.options.upsampling
        
        # decide whath PSF to use
        if psf_input_method is None: psf_input_method='none'
        psf_input_method = psf_input_method.lower()
        psf_function = {
            'psfex': self.get_psfex_psf,
            'none': self.get_fake_psf,
            'collected-psfex':self.get_collected_psfex_psf,
            'bundled-psfex': self.get_bundled_psfex_psf,
            'great_des': self.get_great_des_psf,
            'fixed-gaussian':self.get_fixed_gaussian_psf,
        }.get(psf_input_method)

        if psf_function is None:
            raise I3ParameterError('"%s" unknown PSF input method' % psf_input_method)

        psf = psf_function(iobj, iexp)
        return psf


    def get_i3transform(self, iobj, exposure_id, stamp_size):
        # Build the transform from sky to image coordinates
        from . import structs
        transform = structs.Transform_struct()
        transform.ra0 = 0.0  # these are not used in this case
        transform.dec0 = 0.0
        transform.cosdec0 = 0.0 
        transform.x0 = self['cutout_col'][iobj][exposure_id]+0.5
        transform.y0 = self['cutout_row'][iobj][exposure_id]+0.5

        dudcol=self['dudcol'][iobj][exposure_id]
        dudrow=self['dudrow'][iobj][exposure_id]
        dvdcol=self['dvdcol'][iobj][exposure_id]
        dvdrow=self['dvdrow'][iobj][exposure_id]

        B = np.array([ [dudcol,dudrow],[dvdcol,dvdrow]])
        A = np.linalg.inv(B)
        transform.A[0][0] = A[0,0]
        transform.A[0][1] = A[0,1]
        transform.A[1][0] = A[1,0]
        transform.A[1][1] = A[1,1]
        #print 'transform det = ', np.linalg.det(B)
        #print 'A = ', A
        return transform


    def i3_save_source_paths(self, filename):
        """ Save the list of source paths into a catalog with indices"""
        outfile = open(filename, 'w')
        for i,path in enumerate(self.i3_source_path_list):
            outfile.write("%d    %s\n"% (i,path.strip()))
        outfile.close()



    def get_i3transform_inverse_matrix(self, iobj, exposure_id, stamp_size):

        transform = self.get_i3transform(iobj, exposure_id, stamp_size)
        A = transform.A
        A = [[A[0][0],A[0][1]],[A[1][0],A[1][1]]]
        return np.linalg.inv(A)

    # This next function is taken from wcsutils.py in the ucl_des_shear repository
    def convert_g_image2sky(self, iobj, exposure_id, stamp_size, g1image, g2image):
        """Return the ellipticity (g1sky, g2sky) in sky coordinates corresponding to the input
        (g1image, g2image).

        Uses the ellipticity convention |g| = (a-b)/(a+b).
        
        Currently only works for scalar input g1image, g2image, but can be called in list
        comprehensions.
        """
        Aimage2sky = self.get_i3transform_inverse_matrix(iobj, exposure_id, stamp_size)
        e1image, e2image = utils.convert_e_linear_to_quadratic(g1image, g2image)
        # Build the ellipticity matrix
        Ei = np.array(((1. + e1image, e2image), (e2image, 1. - e1image)))
        # Perform the transformation
        Es = np.dot(np.dot(Aimage2sky, Ei), Aimage2sky.T)
        # Extract the ellipticities and convert back to the linear |g|=(a-b)/(a+b) ellips
        Estrace = Es.trace()
        e1sky, e2sky = (Es[0, 0] - Es[1, 1]) / Estrace, 2. * Es[0, 1] / Estrace
        g1sky, g2sky = utils.convert_e_quadratic_to_linear(e1sky, e2sky)
        return g1sky, g2sky
        
    def convert_g_sky2image(self, iobj, exposure_id, stamp_size, g1sky, g2sky):
        """Return the ellipticity (g1image, g2image) in image coordinates corresponding to the input
        (g1sky, g2sky).

        Uses the ellipticity convention |g| = (a-b)/(a+b).
        
        Currently only works for scalar input g1sky, g2sky, but can be called in list
        comprehensions.
        """
        transform = self.get_i3transform(iobj, exposure_id, stamp_size)
        Asky2image = np.array(transform.A)
        e1sky, e2sky = utils.convert_e_linear_to_quadratic(g1sky, g2sky)
        # Build the ellipticity matrix
        Es = np.array(((1. + e1sky, e2sky), (e2sky, 1. - e1sky)))
        # Perform the transformation
        Ei = np.dot(np.dot(Asky2image, Es), Asky2image.T)
        # Extract the ellipticities and convert back to the linear |g|=(a-b)/(a+b) ellips
        Eitrace = Ei.trace()
        e1image, e2image = (Ei[0, 0] - Ei[1, 1]) / Eitrace, 2. * Ei[0, 1] / Eitrace
        g1image, g2image = utils.convert_e_quadratic_to_linear(e1image, e2image)
        return g1image, g2image


    def select_exposures(self, iobj, first, last):
        #Use all exposures if not using flags
        if not self.options.use_image_flags:
            return range(first,last)

        #Return a list of indices of exposures to select.
        #Get the image flag for each exposure
        flags = [
            self.get_source_info(iobj, i)['image_flags'] 
            for i in xrange(first, last)
        ]

        orig_rows = [
            self['orig_row'][iobj][i]
            for i in xrange(first, last)
        ]

        orig_cols = [
            self['orig_col'][iobj][i]
            for i in xrange(first, last)
        ]


        if False:
#        if options.glowing_edge:
            row_min = self.options.glowing_edge
            row_max = DES_ROWMAX - self.options.glowing_edge
            col_min = self.options.glowing_edge
            col_max = DES_COLMAX - self.options.glowing_edge
        else:
            row_min = -np.inf
            row_max = np.inf
            col_min = -np.inf
            col_max = np.inf

        if self.options.tiling_split_file:
            using_tiling = []
            for i in xrange(first, last):
                exposure = int(self.get_ccd_info(iobj,i)[1])
                use = (TILING_SPLIT.tiling_is_even(exposure)==self.options.tiling_split_even)
                if self.options.verbosity>3:
                    print "TILING: {} {} {} {}".format(iobj, i, exposure, use)
                using_tiling.append(use)
        else:
            using_tiling = [True for f in flags]

        selected_exposures = [
            i+1 for i in xrange(len(flags))
            if
                (flags[i]==0)
                and
                row_min < orig_rows[i] < row_max
                and 
                col_min < orig_cols[i] < col_max
                and
                using_tiling[i]
        ]


    
        if self.options.single_exposure_only!=-1:
            exp_to_use = self.options.single_exposure_only
            num_exp = len(selected_exposures)-1
            if num_exp<exp_to_use:
                print 'ERROR: exposure %d was chosen to fit, but object %d has only %d exposures'%(exp_to_use, iobj, num_exp)
            else:
                print 'Using exposure %d only.'%exp_to_use
            selected_exposures = selected_exposures[exp_to_use:exp_to_use+1]
            

        return selected_exposures
    
    def get_ccd_info(self, iobj, iexp):
        try:
            source_path = self.get_source_path(iobj, iexp)
        except:
            return 0, "N/A"
        source_base = os.path.basename(source_path).split('.')[0]
        decam, exposure_name, ccd = source_base.split("_")
        ccd = int(ccd)
        return ccd, exposure_name
 
    def run_uberseg(self, iobj, iexp, weight, segid_from_number=False):
        uber_weight = self.get_cweight_cutout_nearest(iobj, iexp, segid_from_number=segid_from_number)
        #uber_weight is the same as weight but with zeros
        #at ubersegged-out pixels.  We would like to:
        # 1) get a mask just showing which pixels are ubersegged
        # i.e. they have zero weight just because of uberseg
        uber_mask = (~((uber_weight==0) & (weight!=0))).astype(float)
        # 2) set weight to uber_weight
        weight[:] = uber_weight
        return uber_mask

    def get_im3shape_weight(self, iobj, iexp, image):
        weight = self.get_cweight_cutout(iobj, iexp, segid_from_number=self.options.segid_from_number)

        if self.options.ignore_meds_weight:
            weight_max = weight.max()
            weight.fill(weight_max)


        #blank out the weight mask where other objects
        #are assigned
        if self.options.uberseg:
            uber_mask=self.run_uberseg(iobj,iexp,weight,segid_from_number=self.options.segid_from_number)
        else:
            uber_mask=None

        #Rescale weights (i.e. check they are correctly normalized)
        if self.options.rescale_weight:
            utils.rescale_weight_edge(image, weight)

        # set the negative weights to zero (those are flags)
        weight_max = weight.max()
        weight[weight<weight_max*1e-4]=0
        return weight, uber_mask

    def get_radec(self, iobj):
        try:
            return self['ra'][iobj], self['dec'][iobj]
        except (ValueError,KeyError):
            return 0.0, 0.0


    def get_exposure_info(self, iobj, iexp):

        image = self.get_cutout(iobj, iexp)
        self.options.stamp_size=image.shape[0]

        #Basic images
        psf = self.get_psf(iobj, iexp)

        if (psf is BlacklistedPSF) and (self.options.reject_any_blacklisted):
            raise I3RejectingAnyBlacklist("Blacklist for iobj {}: exposure{} and reject_any_blacklisted".format(iobj, iexp))

        #Shortcut: use None to indicate missing PSF
        if psf is None:
            if self.options.verbosity>3:
                print "No PSF found for", iobj, iexp
            return None
        elif psf is BlacklistedPSF:
            if self.options.verbosity>3:
                print "PSF blacklisted for", iobj, iexp
            return None



        weight, uber_mask = self.get_im3shape_weight(iobj, iexp, image)



        #The celestial->pixel transform
        stamp_size = image.shape[0]
        transform = self.get_i3transform(iobj, iexp, stamp_size)

        #Auxiliary diagnostic data
        ccd, exposure_name = self.get_ccd_info(iobj, iexp)
        orig_row = self['orig_row'][iobj][iexp]
        orig_col = self['orig_col'][iobj][iexp]

        info = {
            'image' : image,
            'weight' : weight,
            'psf' : psf,
            'uber_mask' : uber_mask,
            'stamp_size' : stamp_size,
            'orig_row' : orig_row,
            'orig_col' : orig_col,
            'exposure_name':exposure_name,
            'ccd': ccd,
            'transform':transform,
            'iexp':iexp,
            'iobj':iobj,
        }

        return info


    def get_im3shape_inputs(self, iobj):
        """
        @brief analyse a multi-exposure galaxy 
        @param psf_input_method choose from {psfex,single,none}
        @param iobj id of the object in the MEDS file 
        """
        #Get the number of exposures from the MEDS file
        n_image = self.get_cat()['ncutout'][iobj]
        box_size = self.get_cat()['box_size'][iobj]
        if box_size>100:
            raise I3StampTooLarge("Rejecting object {} as box_size={}".format(iobj, box_size))
        ID = self.get_number(iobj)
        coadd_objects_id = self['id'][iobj]

        first=1
        last=n_image

        #Choose which exposures we want and 
        #get all the info for them
        selection = self.select_exposures(iobj, first, last)
        if self.options.verbosity>4:
            print "Object selection:", selection
        exposures = [self.get_exposure_info(iobj, iexp)
            for iexp in selection]



        #Get a few bits and bobs of metadata
        inputs = I3MultiExposureInputs()
        inputs.meta['ID'] = coadd_objects_id
        inputs.meta['iobj'] = iobj
        inputs.meta['row_id'] = ID
        inputs.meta['tilename'] = self.tilename
        inputs.meta['stamp_size'] = self.options.stamp_size
        ra, dec = self.get_radec(iobj)
        if ra is not None:
            inputs.meta['ra'] = ra
            inputs.meta['dec'] = dec



        #Choose which exposures to use and save them
        selection_bitmask = 0
        for e,exposure in zip(selection,exposures):
            if exposure is not None:
                selection_bitmask |= (1<<e)
                inputs.append(exposure)

        # e.g. rrrr for 4 exposures if this is r-band
        # makes it easier to combine
        inputs.meta['bands'] = self.get_band() * (len(inputs))

        if self.options.reject_outliers:
            inputs.meta['nreject'] = meds.reject_outliers(inputs.all('image'), inputs.all('weight'))
        else:
            inputs.meta['nreject']=0


        return inputs


class I3NBCMEDS(I3MEDS):
    def get_im3shape_inputs(self, iobj):
        inputs = super(I3NBCMEDS, self).get_im3shape_inputs(iobj)
        # parse self.saved_filename to get the run number and shear number
        # #add to the 
        _,_,run_num,shear_num,_ = os.path.split(self.saved_filename)[1].split('.')
        #avoid weird python octal interpretation if number starts with 0
        run_num = run_num.lstrip('0')
        if run_num:
            run_num = int(run_num)
        else:
            run_num = 0
        #strip off 'g' and again avoid 0 problem
        shear_num = shear_num[1:].strip('0')
        if shear_num:
            shear_num = int(shear_num)
        else:
            shear_num = 0
        #ID looks like:
        # RRR0GG0NNNN
        #where R is run_num from 0-199
        #G is shear num from 0-7 (in this case)
        inputs.meta['ID'] = inputs.meta['ID'] + 100000*shear_num+100000000*run_num
        return inputs

    def get_im3shape_weight(self, iobj, iexp, image):
        #current NBC files have a constant weight that we fill
        #in here to avoid more disc access
        return np.ones_like(image)*0.00390625, None
