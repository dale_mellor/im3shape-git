from py3shape.analyze_meds import main
from py3shape.royal_navy import Mariner, Captain
from py3shape.utils import find_tilename
import argparse
import sys
import mpi4py.MPI
import os
import errno
import numpy as np

def mkdir(path):
    #This is much nicer in python 3.
    try:
        os.makedirs(path)
    except OSError as error:
        if error.errno == errno.EEXIST:
            if os.path.isdir(path):
                #error is that dir already exists; fine - no error
                pass
            else:
                #error is that file with name of dir exists already
                raise ValueError("Tried to create dir %s but file with name exists already"%path)
        elif error.errno == errno.ENOTDIR:
            #some part of the path (not the end) already exists as a file 
            raise ValueError("Tried to create dir %s but some part of the path already exists as a file"%path)
        else:
            #Some other kind of error making directory
            raise


parser=argparse.ArgumentParser()
parser.add_argument("meds_list_file", help="Text file with list of meds files")
parser.add_argument("ini_file", help="Im3shape parameter file")
parser.add_argument("catalog", help="Catalog file/table or 'all' for all objects")
parser.add_argument("output_base_dir", help="Directory all output will go into subdirs of")
parser.add_argument("number_groups", type=int, help="Number of groups to split into (each meds file will be split among a group)")

args = parser.parse_args()

class MedsCaptain(Captain):
    def captains_report(self, reports):
        #The captain receives a list of arrays from the sailors containing their
        #results - the coadd_objects_id and a status number
        report = np.vstack(reports)

        #We just report the number of galaxies analyzed by this ship.
        return len(report)

def get_output_dirname(meds_files):
    tilename = find_tilename(meds_files[0])
    output_dir = os.path.join(args.output_base_dir, tilename)
    return output_dir

def generate_tasks(meds_list_file):
    meds_list = [line.strip() for line in open(meds_list_file) if line.strip() and not line.startswith('#')]
    tasks = []
    for meds in meds_list:
        meds_files = meds.split()
        #can have multiple meds files per line
        #use the first one to get the tile name
        output_dir = get_output_dirname(meds_files)
        mkdir(output_dir)
        #store the list of meds files as the task
        tasks.append(meds_files)
    return tasks


mariner = Mariner.assign_rank(args.number_groups, captain_class=MedsCaptain)

if mariner.is_admiral:
    tasks = generate_tasks(args.meds_list_file)
else:
    tasks = None



def work_function(meds, rank, size):
    output_dir = get_output_dirname(meds)
    default_catalog = os.path.join(output_dir, "catalog.txt")
    if os.path.exists(default_catalog):
        catalog = default_catalog
    else:
        catalog = args.catalog
    command_line = [meds, args.ini_file, catalog, output_dir+"/"+str(rank), size, rank, False]
    print "Running:", command_line
    sys.stdout.flush()
    main(*command_line)


mariner.perform_duties(tasks, work_function)

