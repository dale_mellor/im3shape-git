from .image import Image
from .options import Options
from .dataset import Dataset
from .analyze import analyze, PosteriorCalculator
from .utils import small_round_psf_image
from .pyfits import pyfits
import numpy as np
import pylab
import scipy.optimize as opt
import time


def demo1():
    options = Options()
    options.read("./example/params.ini")

    galaxy = pyfits.getdata("./example/image.fits")[0:options.stamp_size, 0:options.stamp_size]
    psf = Image(small_round_psf_image((options.stamp_size+options.padding)*options.upsampling))
    result, model = analyze(galaxy, psf, options)

    print "Result in python: "
    print result.get_params()

    print "Plotting now."
    pylab.subplot(131)
    pylab.imshow(galaxy/galaxy.sum())
    pylab.title("Galaxy")

    pylab.subplot(132)
    pylab.imshow(model)
    pylab.title("Model")

    pylab.subplot(133)
    pylab.imshow(model-galaxy/galaxy.sum())
    pylab.title("Difference")

    pylab.show()

def demo2():
    options = Options("./example/params.ini")

    ny = nx = options.stamp_size
    Nx = Ny = (nx+options.padding) * options.upsampling
    psf_e1, psf_e2 = 0.019, -0.007


    image_array = pyfits.getdata("./example/image.fits")[:nx,:ny]
    norm = image_array.sum()
    image_array /= norm
    options.noise_sigma /= norm

    image = Image(image_array)

    psf = Image(Nx, Ny)
    psf.add_moffat(2.85*options.upsampling, psf_e1, psf_e2, Nx/2.,  Ny/2., 3.0)
    psf_array = psf.array() / psf.sum()
    psf = Image(psf_array)
    dataset = Dataset(options, image, psf)
    posterior = PosteriorCalculator(options, dataset)

    start = posterior.starting_vector
    t1 =  time.time()
    approx = opt.fmin(posterior, start, ftol=1e-2)
    img1 = posterior.model.model_image.array()
    better = opt.fmin_bfgs(posterior, approx, gtol=1.0)
    t2 = time.time()
    img2 = posterior.model.model_image.array()
    print "Time taken ", t2 - t1
    print approx
    print better
    pylab.subplot(121)
    pylab.imshow(img1)
    pylab.subplot(122)
    pylab.imshow(img2)
    pylab.show()



if __name__=="__main__":
    demo2()
