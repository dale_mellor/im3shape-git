#include "i3_image.h"
#include "i3_model_tools.h"


int main(){
	int N=500;
	
	
	i3_flt e1 = 0.1;
	i3_flt e2 = -0.4;
	i3_flt x0 = N/2.;
	i3_flt y0 = N/2.;
	i3_flt bulge_amplitude = 1.0;
	i3_flt disc_amplitude = 0.5;
	i3_flt bulge_radius = 3.0;
	i3_flt disc_radius = 6.0;
	
	i3_flt bulge_sersic_index = 1.0;
	i3_flt disc_sersic_index = 4.0;
	
	
	i3_image * image = i3_image_create(N,N);

	i3_image_zero(image);
	i3_add_real_space_sersic(image, bulge_radius*bulge_radius, e1,e2, bulge_amplitude,x0,y0, bulge_sersic_index);
	i3_image_save_text(image,"bulge.txt");
	
	i3_image_moments mom;
	i3_image_compute_moments(image,&mom);
	printf("e1 = %f\ne2 = %f\n",mom.e1,mom.e2);

	i3_image_zero(image);
	i3_add_real_space_sersic(image, disc_radius*disc_radius, e1,e2, disc_amplitude,x0,y0, disc_sersic_index);
	i3_image_save_text(image,"disc.txt");
	
	i3_image_zero(image);
	i3_add_real_space_sersic(image, bulge_radius*bulge_radius, e1,e2, bulge_amplitude,x0,y0, bulge_sersic_index);
	i3_add_real_space_sersic(image, disc_radius*disc_radius, e1,e2, disc_amplitude,x0,y0, disc_sersic_index);
	i3_image_save_text(image,"bulge_disc.txt");
	
	
}




