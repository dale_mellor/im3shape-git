#include "i3_image.h"
#include "i3_image_fits.h"

int main(){
	int nx = 50;
	int ny = 100;
	
	
	i3_image * image = i3_image_create(nx,ny);
	for(int i=0;i<image->n;i++) image->data[i] = i;
	i3_image_save_fits(image,"contig.fits");
}
