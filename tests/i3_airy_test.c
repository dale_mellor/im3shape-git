#include "i3_psf.h"
#include "i3_image_fits.h"

int main(){
	int nx = 480;
	int ny = nx;
	i3_flt fwhm = 40;
	i3_flt e1 = 0.3;
	i3_flt e2 = 0.;
	i3_flt truncation = 1000.;

        i3_flt x = 300.;
        i3_flt y = 100.;
	
	i3_image * airy = i3_image_create(nx,ny);
	i3_image_zero(airy);
	i3_image_add_truncated_airy(airy, x, y, fwhm, e1, e2, truncation);
	i3_image_save_text(airy, "airy.txt");
        i3_image_save_fits(airy, "airy.fits");
       
}
