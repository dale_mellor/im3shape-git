#include "i3_image.h"
#include "i3_load_data.h"
#include "stdio.h"
#include "stdlib.h"


int main(int argc, char *argv[]){
	if (argc<2){
		fprintf(stderr,"Usage: %s filename.fits\n",argv[0]);
		exit(1);
	}
	char * saved_image_1 = "i3_image_test_ouput1.txt";
	char * saved_image_2 = "i3_image_test_ouput2.txt";
	i3_image * image = i3_read_fits_image(argv[1]);
	if (!image) return 1;
	printf("Successfully loaded primary image in %s\n",argv[1]);
	i3_image * subimage = i3_image_copy_part(image,0,0,50,50);
	if (!subimage) {fprintf(stderr,"Failed to copy subimage\n"); return 1;}
	if (subimage) printf("Successfully got sub-image of size %ldx%ld.\n",subimage->nx,subimage->ny);
	
	int saving_error = i3_image_save_text(subimage, saved_image_1);
	if (saving_error) {fprintf(stderr,"Failed to save image\n"); return 2;}
	printf("Sucessfully saved image section to file %s\n",saved_image_1);
	
	i3_image * PSF = i3_image_create(subimage->nx,subimage->ny);
	for(int i=0;i<PSF->n;i++) PSF->data[i]=0;
	PSF->row[25][25]=1.0;
	i3_image_shift_center(PSF,25,25);
	i3_cpx * PSF_FT = i3_image_fft(PSF);
	
	i3_image_convolve_fft(subimage,PSF_FT);
	saving_error = i3_image_save_text(PSF, "psf.txt");

	saving_error = i3_image_save_text(subimage, saved_image_2);
	if (saving_error) {fprintf(stderr,"Failed to save image 2\n"); return 3;}
	printf("Sucessfully saved image section to file %s\n",saved_image_2);
	
	
	i3_image_destroy(image);
	i3_image_destroy(subimage);
	i3_image_destroy(PSF);
	
	return 0;
}
