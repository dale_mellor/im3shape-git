#include "i3_model_tools.h"
#include "i3_image.h"

int main(){
	i3_image * image = i3_image_create(256, 256);
	i3_image_zero(image);
	i3_add_real_space_king_profile(image, 20.0, 20.0, 0.0, 0.0, 1.0, 128.0, 128.0);
	i3_image_save_fits(image, "king.fits");


}