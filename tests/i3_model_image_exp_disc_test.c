#include "i3_image.h"
#include "i3_model_tools.h"
#include "i3_great.h"


int main(){
	int N=40;
	int nx=N;
	int ny=N;
	
	i3_flt e1 = 0;
	i3_flt e2 = 0.2;
	//i3_flt e1 = 0;
	//i3_flt e2 = 0;
	i3_flt x0 = N/2. +0.8;
	i3_flt y0 = N/2. +0.8;
	int u = 5; /* upsampling */
	i3_flt sersic_exp = 4.f;
	//i3_flt fwhm = 6.0;
	//i3_flt ab_exp = i3_fwhm_to_ab(fwhm*u,sersic_exp);
	i3_flt radius = 4.0*u;
	i3_flt amplitude = 1.f; 
	i3_flt truncation_factor = 4.0;
	
	
	/* Generate the high-resolution image with upsampling*(npix+1) pixels per side. */
	/* Allocate space for the image and zero it.*/	
	i3_image * high_resolution_image = i3_image_create((nx+1)*u,(ny+1)*u);	
	i3_image_zero(high_resolution_image);
	
	/* Add the exponential disc */
	//i3_add_real_space_sersic(radius*radius, e1, e2, amplitude, x0*u, y0*u, sersic_exp, high_resolution_image);
	i3_add_real_space_sersic_truncated_radius(high_resolution_image, radius*radius, e1, e2, amplitude, x0*u, y0*u, sersic_exp, truncation_factor);

    /* reset central pixel */
	i3_flt r0 = radius;
	i3_flt truncation = truncation_factor*r0;
	i3_flt analytic_flux = i3_sersic_flux_to_radius(sersic_exp, amplitude, radius*radius, truncation);
	i3_flt current_flux = i3_image_sum(high_resolution_image);
	int peak_row = (int) (x0+1)*u; // why the plus 1? am not sure (SLB)
	int peak_col = (int) (y0+1)*u;
	high_resolution_image->row[peak_row][peak_col] += (analytic_flux-current_flux);

	// Build the kernel, which combines the top-hat and psf convolutions
	// First, choose which of the three Great08 PSFs to use.
	// Then build a gernel based on that and the specified sizes and upsampling.
	int psf_set = 1;
	i3_fourier * kernel = i3_build_great08_kernel(psf_set,N,N,u);

	//i3_image_save_text(kernel,"great08_kernel_w_tophat.txt");

    /* need to make the model_image space */
	i3_image * model_image = i3_image_create(nx,ny);
	i3_image_zero(model_image);

	/* Convolve with the pre-prepared point spread function and Sinc kernel, and downsample. */
	i3_image_convolve_downsample_into(high_resolution_image, kernel, model_image);
	i3_image_destroy(high_resolution_image);
		
			
	i3_image_save_fits(model_image,"model_image_exp_disc.fits");
	
	
}




