#include "i3_image.h"
#include "i3_load_data.h"
#include "stdio.h"
#include "stdlib.h"
#include "i3_model.h"
#include "i3_model_tools.h"
#include "i3_psf.h"
#include <time.h>





int main(int argc, char *argv[]){

// init

	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);

// stamp parameters

	int N_pix = 99;
	int N_obs = 69; 
	int N_sub = 15;
	int N_cut = N_pix - N_obs;
	
// psf parameters

	i3_flt psf_sersic_n = 0.5f; 
	i3_flt psf_A=1.0f;
	i3_flt psf_x0=N_pix*N_sub/2; 
	i3_flt psf_y0=N_pix*N_sub/2; 
	i3_flt psf_fwhm = 2.85f*N_sub;
	i3_flt psf_ab = i3_fwhm_to_ab(psf_fwhm,psf_sersic_n);
	i3_flt psf_e1 = 0.0f;
	i3_flt psf_e2 = 0.0f;
	i3_image * image_psf_l = i3_image_create(N_pix*N_sub,N_pix*N_sub);	i3_image_zero(image_psf_l);
	i3_add_real_space_sersic( image_psf_l, psf_ab,psf_e1,psf_e2, psf_A, psf_x0, psf_y0, psf_sersic_n );
	i3_image_save_text( image_psf_l,"image_psf_l.txt" );
	
// get galaxy

	i3_flt gal_sersic_n = 1.f;
 	i3_flt gal_A=1.0f;
 	i3_flt gal_x0= N_pix*N_sub/2; 
	i3_flt gal_y0= N_pix*N_sub/2; 
 	i3_flt gal_fwhm = 2.0f*N_sub;
 	i3_flt gal_ab = i3_fwhm_to_ab(gal_fwhm,gal_sersic_n);
 	i3_flt gal_e1 = 0.5f;
 	i3_flt gal_e2 = 0.0f;
	i3_image * image_gal_l = i3_image_create(N_pix*N_sub,N_pix*N_sub);	i3_image_zero(image_gal_l);
	i3_add_real_space_sersic(image_gal_l,  gal_ab, gal_e1 , gal_e2 , gal_A , gal_x0 , gal_y0 , gal_sersic_n  );
	i3_image_save_text( image_gal_l,"image_gal_l.txt" );

// get sinc

	i3_image * image_pix_l = i3_image_get_top_hat( N_pix,N_pix,N_sub );
	i3_image_save_text( image_pix_l,"image_pix_l.txt" );

// get small ones

	i3_image * image_gal_s = i3_image_copy_part( image_gal_l , (N_cut/2)*N_sub, (N_cut/2)*N_sub, N_obs*N_sub, N_obs*N_sub ); 
	i3_image * image_psf_s = i3_image_copy_part( image_psf_l , (N_cut/2)*N_sub, (N_cut/2)*N_sub, N_obs*N_sub, N_obs*N_sub );
	i3_image * image_pix_s = i3_image_copy_part( image_pix_l , (N_cut/2)*N_sub, (N_cut/2)*N_sub, N_obs*N_sub, N_obs*N_sub );

// prep

	i3_image * image_psf_l_c = i3_image_centered( image_psf_l );
	i3_image_save_text( image_psf_l_c,"image_psf_l_c.txt" ); 

	i3_image * image_pix_l_c = i3_image_centered( image_pix_l );
	i3_image_save_text( image_pix_l_c,"image_pix_l_c.txt" );

	i3_image * image_gal_l_c = i3_image_centered( image_gal_l );
	i3_image_save_text( image_gal_l_c,"image_gal_l_c.txt" );	

	i3_image * image_psf_s_c = i3_image_centered( image_psf_s );
	i3_image_save_text( image_psf_s_c,"image_psf_s_c.txt" ); 

	i3_image * image_pix_s_c = i3_image_centered( image_pix_s );
	i3_image_save_text( image_pix_s_c,"image_pix_s_c.txt" );

	i3_image * image_gal_s_c = i3_image_centered( image_gal_s );
	i3_image_save_text( image_gal_s_c,"image_gal_s_c.txt" );	
	

// get the FTs
			
	i3_fourier * fourier_psf_l = i3_image_fourier( image_psf_l_c );
	i3_fourier * fourier_pix_l = i3_image_fourier( image_pix_l_c );
	
	i3_fourier * fourier_psf_s = i3_image_fourier( image_psf_s_c );
	i3_fourier * fourier_pix_s = i3_image_fourier( image_pix_s_c );

// combine kernels
	
	i3_fourier * fourier_ker_l = i3_fourier_create(N_pix*N_sub,N_pix*N_sub);
	i3_multiply_fourier_fourier_into(fourier_pix_l,fourier_psf_l,fourier_ker_l);

	i3_fourier * fourier_ker_s = i3_fourier_create(N_obs*N_sub,N_obs*N_sub);
	i3_multiply_fourier_fourier_into(fourier_pix_s,fourier_psf_s,fourier_ker_s);

// convolve
	
	i3_image_convolve_fourier( image_gal_l, fourier_ker_l );
	i3_image_save_text(  image_gal_l ,"image_obs_l.txt" );
	
	i3_image_convolve_fourier( image_gal_s, fourier_ker_s );
	i3_image_save_text(  image_gal_s ,"image_obs_s.txt" );
	
// now cut out the large to be the same size as small

	i3_image * image_gal_l_cut = i3_image_copy_part( image_gal_l , (N_cut/2)*N_sub, (N_cut/2)*N_sub, N_obs*N_sub, N_obs*N_sub );	
	i3_image_save_text(  image_gal_l_cut ,"image_obs_l_cut.txt" );
	
	return 0;


/*
Result of checking the difference between padded and unpadded images
relative differnece defined as
max(abs(image_obs_l_cut(:)-image_obs_s(:)))/max(image_obs_s(:))
big	small	diff_e1_0.5	diff_e1_0.5
99	29	1e-03	3e-03
99	39	4e-04	6e-05
99	49	5e-05	4e-06
99	59	8e-06   7e-07
99	69	1e-06	3e-07



*/

}




