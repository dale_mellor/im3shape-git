#include "i3_psf.h"
#include "i3_image_fits.h"

int main(){
	int npix_low_resolution=50;
	int upsampling=5;
	i3_flt x0_low_resolution = 10.0;
	i3_flt y0_low_resolution = 35.0;
	// Great08 Star value.  See below
	i3_flt moffat_radius_low_resolution = 2.90599965;
	i3_flt moffat_beta = 3.0;

	int npix_high_resolution=npix_low_resolution*upsampling;
	i3_flt x0_high_resolution = x0_low_resolution*upsampling;
	i3_flt y0_high_resolution = y0_low_resolution*upsampling;
	i3_flt moffat_radius_high_resolution = moffat_radius_low_resolution * upsampling;
	
	//Great08 star set001 values.
	i3_flt e1 = -0.3;
	i3_flt e2 = 0.3;

	i3_image * moffat = i3_image_create(npix_high_resolution, 2 * npix_high_resolution);
	i3_image_zero(moffat);
	i3_image_add_moffat(moffat,x0_high_resolution,y0_high_resolution, moffat_radius_high_resolution, e1, e2, moffat_beta);
	i3_image_save_text(moffat,"moffat.txt");
        i3_image_save_fits(moffat, "moffat.fits");
	i3_image * moffat_downsampled = i3_image_downsample(moffat,npix_low_resolution,npix_low_resolution);
	i3_image_save_text(moffat_downsampled,"moffat_downsampled.txt");
	
	
}


// NB FWHM of moffat with radius 1 is 1.019649
// FWHM for great08 is 2.85 pixels
// So r_d = 2.90599965



