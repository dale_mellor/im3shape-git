"""

This isn't really a proper setup script.

It invokes make to use the old im3shape build system and then
copies over the resulting shared library.


"""

import os
import glob
import shutil
from distutils.core import setup, Extension
from distutils.command.install import install as DistutilsInstall
from distutils.command.build import build as DistutilsBuild
from distutils.command.clean import clean as DistutilsClean
import sys

scripts=['bin/im3shape.py']

def make_and_copy_library():
    status = os.system("make setup")
    assert status==0
    shutil.copy("lib/libim3.so", "py3shape")

def make_clean():	
    status = os.system("make clean")
    assert status==0

class MyBuild(DistutilsBuild):
    def run(self):
    	make_and_copy_library()
        DistutilsBuild.run(self)


class MyInstall(DistutilsInstall):
    def run(self):
    	make_and_copy_library()
        DistutilsInstall.run(self)

class MyClean(DistutilsClean):
    def run(self):
    	make_clean()
        DistutilsClean.run(self)

cmdclass={
  'build':   MyBuild, 
  'install': MyInstall,
  'clean':   MyClean,
 }


setup(name="im3shape", 
      version="1.0",
      description="Galaxy shape fitting toolkit",
      author="The Im3shape Team",
      author_email="joezuntz@googlemail.com",
      packages=['py3shape', 'py3shape.models'],
      cmdclass=cmdclass,
      url='https://bitbucket.org/joezuntz/im3shape',
      package_data={'py3shape':['libim3.so']},
      scripts=scripts)
